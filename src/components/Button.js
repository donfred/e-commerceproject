import styled from "styled-components"

export const ButtonContainer = styled.button`
                    outline: none !important;
                    text-transform: capitalize;
                    font-size:1.0rem;
                    background: transparent;
                    border-radius: 0.5rem;
                    border: 0.05rem solid var(--lightGreen) !important;
                    border-color: ${props=>props.colch ?"var(--mainYellow) !important" :"var(--lightGreen)"};
                    color: ${props=>props.colch ?"var(--mainYellow)" :"var(--lightGreen)"};
                    padding: 0.2rem 0.5rem;
                    cursor: pointer;
                    margin: 0.2rem 0.5rem 0.2rem 0;
                    transition: all 0.5s ease-in-out;
                    &:hover{
                        background: ${props=>props.colch ?"var(--mainYellow) " :"var(--lightGreen)"};
                        color: var(--mainGreen)
                    }

        `