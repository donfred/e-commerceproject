import React, { Component } from 'react';
import {Link} from "react-router-dom"
import logo from "../logo.svg";
import styled from "styled-components"
import {ButtonContainer} from "./Button"
import {ProductConsumer} from "../context";
class Navbar extends Component {
    render() {
        
        return (
            <ProductConsumer>
                {(value)=>{
                    const {cart} =value
                    const itemQ = cart.length
                    return(
                        <NavbarWrap className="navbar  navbar-expand-sm navbar-dark px-sm-5">
                    {/* 
                        https://www.iconfinder.com/icons/1243689/call_phone_icon
                        Creative Commons (Attribution 3.0 Unported);
                        https://www.iconfinder.com/Makoto_msk */
                    }
                    <Link to="/">
                        <img src={logo} alt="commerce-logo" className="navbar-brand" />
                    </Link>
                    <ul className="navbar-nav align-items-center">
                        <li className="nav-item ml-5">
                            <Link className="nav-link" to="/">
                                prodwi yo
                            </Link>
                        </li>
                    </ul>
                    <Link to="/cart" className="ml-auto">

                        <ButtonContainer className="border">
                            <span className="panier">
                                <i className="fas fa-cart-plus"> </i>
                                {itemQ>0 && <span className="text-dangerous">{itemQ}
                                    </span>}
                                    
                            </span>       
                               
                            {"     "}
                            Panyen mwen
                            
                        </ButtonContainer>
                    </Link>
                </NavbarWrap>
                    );
                }}
                
            </ProductConsumer>
        );
        
    }
    
}

const NavbarWrap = styled.nav`
    background: var(--mainGreen);
    color: #bada55 !important;
    font-size: 1.3rem;//1rem = 16px
    text-transform: capitalize;
`

export default Navbar;